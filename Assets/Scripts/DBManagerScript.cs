﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DBManagerScript : MonoBehaviour
{
    //Adresse de la BDD
    [SerializeField] private string databaseURL = "http://localhost/bvb_proto_db/";
    private char getDataSeparator = '\t';


    const int getRobotDataSize = 6;
    const int getWeaponDataFromRobotIdSize = 6;
    const int getRobotWinRateSize = 2;

    //Méthode requète getRobotData
    public void getRobotData(int arg_robotId, RobotDBController arg_DBController)
    {
        StartCoroutine(getRobotDataCoroutine(arg_robotId, arg_DBController));
    }

    //Coroutine getRobotData
    IEnumerator getRobotDataCoroutine(int arg_robotId, RobotDBController arg_DBController)
    {
        string loc_getRobotDataURL = databaseURL + "getRobotData.php";
        WWWForm loc_getRobotDataForm = new WWWForm();
        loc_getRobotDataForm.AddField("robotId", arg_robotId);
        WWW loc_dbRequest = new WWW(loc_getRobotDataURL, loc_getRobotDataForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            string[] loc_robotDataString = loc_dbRequest.text.Split(getDataSeparator);
            int[] loc_robotDataInt = new int[getRobotDataSize];

            for (int i = 0; i < loc_robotDataInt.Length; i++)
            {
                int loc_parsedValue;

                if (int.TryParse(loc_robotDataString[i], out loc_parsedValue))
                {
                    loc_robotDataInt[i] = loc_parsedValue;
                }
                else
                {
                    loc_robotDataInt[i] = 0;
                    Debug.Log("Error: value retrieved is not an integer");
                }
            }

            arg_DBController.returnRobotData = loc_robotDataInt;
            arg_DBController.getRobotDataReturn = true;
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }


    //Méthode requète getWeaponDataFromRobotId
    public void getWeaponDataFromRobotId(int arg_robotId, RobotDBController arg_DBController)
    {
        StartCoroutine(getWeaponDataFromRobotIdCoroutine(arg_robotId, arg_DBController));
    }

    //Coroutine getWeaponDataFromRobotId
    IEnumerator getWeaponDataFromRobotIdCoroutine(int arg_robotId, RobotDBController arg_DBController)
    {
        string loc_getWeaponDataFromRobotIdURL = databaseURL + "getWeaponDataFromRobotId.php";
        WWWForm loc_getWeaponDataFromRobotIdForm = new WWWForm();
        loc_getWeaponDataFromRobotIdForm.AddField("robotId", arg_robotId);
        WWW loc_dbRequest = new WWW(loc_getWeaponDataFromRobotIdURL, loc_getWeaponDataFromRobotIdForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            string[] loc_weaponDataString = loc_dbRequest.text.Split(getDataSeparator);
            int[] loc_weaponDataInt = new int[getWeaponDataFromRobotIdSize];

            for (int i = 0; i < loc_weaponDataInt.Length; i++)
            {
                int loc_parsedValue;

                if (int.TryParse(loc_weaponDataString[i], out loc_parsedValue))
                {
                    loc_weaponDataInt[i] = loc_parsedValue;
                }
                else
                {
                    loc_weaponDataInt[i] = 0;
                    Debug.Log("Error: value retrieved is not an integer");
                }
            }

            arg_DBController.returnWeaponDataFromRobotId = loc_weaponDataInt;
            arg_DBController.getWeaponDataFromRobotIdReturn = true;
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }


    //Méthode requète getRobotWinRate
    public void getRobotWinRate(int arg_robotId, RobotDBController arg_DBController)
    {
        StartCoroutine(getRobotWinRateCoroutine(arg_robotId, arg_DBController));
    }

    //Coroutine getRobotData
    IEnumerator getRobotWinRateCoroutine(int arg_robotId, RobotDBController arg_DBController)
    {
        string loc_getRobotWinRateURL = databaseURL + "getRobotWinRate.php";
        WWWForm loc_getRobotWinRateForm = new WWWForm();
        loc_getRobotWinRateForm.AddField("robotId", arg_robotId);
        WWW loc_dbRequest = new WWW(loc_getRobotWinRateURL, loc_getRobotWinRateForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            string[] loc_robotWinRateString = loc_dbRequest.text.Split(getDataSeparator);
            int[] loc_robotWinRateInt = new int[getRobotWinRateSize];

            for (int i = 0; i < loc_robotWinRateInt.Length; i++)
            {
                int loc_parsedValue;

                if (int.TryParse(loc_robotWinRateString[i], out loc_parsedValue))
                {
                    loc_robotWinRateInt[i] = loc_parsedValue;
                }
                else
                {
                    loc_robotWinRateInt[i] = 0;
                    Debug.Log("Error: value retrieved is not an integer");
                }
            }

            arg_DBController.returnRobotWinrate = loc_robotWinRateInt;
            arg_DBController.getRobotWinrateReturn = true;
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }


    //Méthode requète getRobotWinRate
    public void updateRobotWinrate(int arg_robotId, bool arg_isWinner, RobotDBController arg_DBController)
    {
        StartCoroutine(updateRobotWinrateCoroutine(arg_robotId, arg_isWinner, arg_DBController));
    }

    //Coroutine getRobotData
    IEnumerator updateRobotWinrateCoroutine(int arg_robotId, bool arg_isWinner, RobotDBController arg_DBController)
    {
        string loc_updateRobotWinRateURL;

        if (arg_isWinner)
        {
            loc_updateRobotWinRateURL = databaseURL + "updateRobotWins.php";
        }
        else
        {
            loc_updateRobotWinRateURL = databaseURL + "updateRobotLosses.php";
        }

        WWWForm loc_updateRobotWinRateForm = new WWWForm();
        loc_updateRobotWinRateForm.AddField("robotId", arg_robotId);
        WWW loc_dbRequest = new WWW(loc_updateRobotWinRateURL, loc_updateRobotWinRateForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            arg_DBController.updateRobotWinrateReturn = true;
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }


    //Méthode requète resetRobotWinRate
    public void resetRobotWinrate(int arg_robotId, RobotDBController arg_DBController)
    {
        StartCoroutine(resetRobotWinrateCoroutine(arg_robotId, arg_DBController));
    }

    //Coroutine resetRobotWinRate
    IEnumerator resetRobotWinrateCoroutine(int arg_robotId, RobotDBController arg_DBController)
    {
        string loc_resetRobotWinRateURL = databaseURL + "resetRobotWinrate.php";
        WWWForm loc_resetRobotWinRateForm = new WWWForm();
        loc_resetRobotWinRateForm.AddField("robotId", arg_robotId);
        WWW loc_dbRequest = new WWW(loc_resetRobotWinRateURL, loc_resetRobotWinRateForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            Debug.Log(loc_dbRequest.text);

            arg_DBController.resetRobotWinrateReturn = true;
            Debug.Log("'resetRobotWinRate' Done");
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }


    private char getTableSeparator = '#';
    const int getRobotGenerationTableSize = 7;

    private void Start()
    {
        //getRobotGenerationTable();
        //sendRobotTableToServer();
    }


    //Méthode requète 
    public void getRobotGenerationTable()
    {
        StartCoroutine(getRobotGenerationTableCoroutine());
    }

    //Coroutine 
    IEnumerator getRobotGenerationTableCoroutine()
    {
        string loc_getRobotGenerationTableURL = databaseURL + "AlgoGen/MainAlgoGen.php";
        WWWForm loc_getRobotGenerationTableForm = new WWWForm();
        WWW loc_dbRequest = new WWW(loc_getRobotGenerationTableURL, loc_getRobotGenerationTableForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            string[] loc_RobotGenerationTableString = loc_dbRequest.text.Split(getTableSeparator);

            for (int i = 0; i < loc_RobotGenerationTableString.Length; i++)
            {
                Debug.Log(loc_RobotGenerationTableString[i]);

                string[] loc_RobotDataString = loc_RobotGenerationTableString[i].Split(getDataSeparator);
                int[] loc_robotDataInt = new int[getRobotGenerationTableSize];

                for (int j = 0; j < loc_RobotDataString.Length; j++)
                {
                    int loc_parsedValue;

                    if (int.TryParse(loc_RobotDataString[j], out loc_parsedValue))
                    {
                        loc_robotDataInt[j] = loc_parsedValue;
                        Debug.Log(loc_robotDataInt[j]);
                    }
                    else
                    {
                        Debug.Log("Error: value retrieved is not an integer");
                    }
                }
            }
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }


    //Méthode requète 
    public void sendRobotTableToServer()
    {
        StartCoroutine(sendRobotTableToServerCoroutine());
    }

    //Coroutine 
    IEnumerator sendRobotTableToServerCoroutine()
    {
        string loc_sendRobotTableToServerURL = databaseURL + "AlgoGen/algoGenReturn.php";
        WWWForm loc_sendRobotTableToServerForm = new WWWForm();
        loc_sendRobotTableToServerForm.AddField("robots", "95 55 35 29 15 76 1#100 89 8 57 89 81 1#31 26 50 50 52 6 1#70 36 63 36 78 68 1#46 34 62 39 77 66 1#1 64 59 89 36 32 1#82 65 73 29 95 78 1#35 49 76 80 37 42 1#36 97 63 91 89 59 1#23 5 65 27 20 63 1#99 80 53 95 6 18 1#50 59 41 56 75 21 1#21 54 21 96 44 43 1#34 6 49 82 81 56 1#27 60 26 10 34 11 1#48 19 37 92 13 63 1#62 96 39 37 82 41 1#9 91 2 23 63 39 1#23 71 22 25 99 24 1#74 92 46 63 78 23 1#83 54 51 26 49 28 1#24 33 36 81 20 26 1#16 51 14 42 63 86 1#100 100 30 96 87 52 1#62 3 16 42 7 9 1");
        WWW loc_dbRequest = new WWW(loc_sendRobotTableToServerURL, loc_sendRobotTableToServerForm);

        yield return loc_dbRequest;

        if (loc_dbRequest.error == null)
        {
            //Debug.Log("ça marche!");
        }
        else
        {
            Debug.Log("WWW Error: " + loc_dbRequest.error);
        }
    }
}