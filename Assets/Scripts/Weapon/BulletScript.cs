﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [HideInInspector] public int bulletSpeed;

    void FixedUpdate()
    {
        
    }

    void OnCollisionEnter(Collision objectCollided)
    {
        /*
        if (objectCollided.collider.name != gameObject.name) // detruit les bullets sauf si collision avec une autre bullet
        {
            Destroy(gameObject);
        }
        */
        Destroy(gameObject);
    }
}
