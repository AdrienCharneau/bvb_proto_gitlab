﻿using UnityEngine;

public class RobotDBController
{
    private DBManagerScript DBManager;

    //Variables de la requète getRobotData
    public bool getRobotDataReturn = false;
    public int[] returnRobotData;

    //Variables de la requète getRobotData
    public bool getWeaponDataFromRobotIdReturn = false;
    public int[] returnWeaponDataFromRobotId;

    //Variables de la requète getRobotWinRate
    public bool getRobotWinrateReturn = false;
    public int[] returnRobotWinrate;

    //Variable de la requète updateWinRate
    public bool updateRobotWinrateReturn = false;

    //Variable de la requète resetRobotWinRate
    public bool resetRobotWinrateReturn = false;


    public RobotDBController(DBManagerScript arg_DBManager)
    {
        DBManager = arg_DBManager;
    }


    //  Methode de remise à 0 du winrate du robot
    public void RobotResetWinrate(int arg_robotId, RobotDBController arg_DBController)
    {
        DBManager.resetRobotWinrate(arg_robotId, arg_DBController);
    }

    //  Methode de mise en place des données du robot depuis la base de données
    public void RobotSetupFromDB(int arg_robotId, RobotDBController arg_DBController)
    {
        DBManager.getRobotData(arg_robotId, arg_DBController);
        DBManager.getWeaponDataFromRobotId(arg_robotId, arg_DBController);
        DBManager.getRobotWinRate(arg_robotId, arg_DBController);
    }

    public void RobotUpdateWinrate(int arg_robotId, bool arg_isWinner, RobotDBController arg_DBController)
    {
        DBManager.updateRobotWinrate(arg_robotId, arg_isWinner, arg_DBController);
    }

}
