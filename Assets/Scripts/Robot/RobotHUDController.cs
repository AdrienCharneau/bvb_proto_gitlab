﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class RobotHUDController
{
    // Mise à jour de la Healthbar
    public void updateHealthBar(Image arg_robotHealthBar, int arg_robotCurentStatHp, int robotStatHp)
    {
        float loc_healthBarFillValue = (float)arg_robotCurentStatHp / (float)robotStatHp;
        arg_robotHealthBar.fillAmount = loc_healthBarFillValue;
    }

    // Mise à jour du Score Display
    public void updateWinRateDisplay(Text arg_robotWinRateDisplay, Text arg_robotTotalGamesDisplay, int[] arg_robotWinRateData)
    {
        //float loc_totalRobotGames = Convert.ToSingle(arg_robotWinRateData[0]) + Convert.ToSingle(arg_robotWinRateData[1]);
        int loc_robotTotalGames = arg_robotWinRateData[0] + arg_robotWinRateData[1];

        if (arg_robotWinRateData[0] > 0)
        {
            float loc_robotWins = Convert.ToSingle(arg_robotWinRateData[0]);
            double loc_robotWinrate = Math.Round(loc_robotWins / loc_robotTotalGames, 2) * 100;
            arg_robotWinRateDisplay.text = "Winrate: " + (int)loc_robotWinrate + "%";
        }
        else
        {
            arg_robotWinRateDisplay.text = "Winrate: 0%";
        }

        arg_robotTotalGamesDisplay.text = "Games: " + loc_robotTotalGames;

        //arg_robotWinRateDisplay.text = "Wins: " + arg_robotWinRateData[0];
        //arg_robotTotalGamesDisplay.text = "Losses: " + arg_robotWinRateData[1];
    }
}
