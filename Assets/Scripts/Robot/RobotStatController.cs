﻿using UnityEngine;

public static class RobotStatController
{
    // Mise en place des statistiques du Robot
    public static void StatSetup(Robot arg_robot, RobotWeapon arg_robotWeapon, int[] arg_robotStatsTable, int[] arg_weaponStatsTable)
    {
        arg_robot.statAttack = NormalizeStatValue(arg_robotStatsTable[0]);
        arg_robot.statHp = NormalizeStatValue(arg_robotStatsTable[1]);
        arg_robot.statSpeed = NormalizeStatValue(arg_robotStatsTable[2]);

        arg_robot.behaviorProximity = NormalizeStatValue(arg_robotStatsTable[3]);
        arg_robot.behaviorAgility = NormalizeStatValue(arg_robotStatsTable[4]);
        arg_robot.behaviorAggressivity = NormalizeStatValue(arg_robotStatsTable[5]);

        //arg_robotWeapon.bulletId = NormalizeStatValue(arg_weaponStatsTable[0]);
        arg_robotWeapon.bulletSpeed = NormalizeStatValue(arg_weaponStatsTable[1]);
        arg_robotWeapon.maxRange = NormalizeStatValue(arg_weaponStatsTable[2]);
        arg_robotWeapon.minRange = NormalizeStatValue(arg_weaponStatsTable[3]);
        arg_robotWeapon.rateOfFire = NormalizeStatValue(arg_weaponStatsTable[4]);
        arg_robotWeapon.damageValue = NormalizeStatValue(arg_weaponStatsTable[5]);

        arg_robot.curentStatHp = arg_robot.statHp;
    }

    //Normalisation de valeur de statistique
    public static int NormalizeStatValue(int arg_stat)
    {
        if (arg_stat > 100)
        {
            arg_stat = 100;
        }

        if (arg_stat < 1)
        {
            arg_stat = 1;
        }

        return arg_stat;
    }

    //Normalisation de valeur de statistique courante
    public static int NormalizeCurentHpValue(int arg_stat)
    {
        if (arg_stat > 100)
        {
            arg_stat = 100;
        }

        if (arg_stat < 0)
        {
            arg_stat = 0;
        }

        return arg_stat;
    }
}
