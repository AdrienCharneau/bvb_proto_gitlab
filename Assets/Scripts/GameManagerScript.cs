﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    //  Game Settings
    public gameTypeEnum gameType;
    public bool resetWinrates = false;
    public bool muteAudio = false;
    public float timeScale = 1f;

    // Audio
    public AudioListener audioManager;

    //  Robots Event Scripts
    [SerializeField] private RobotEventScript robotEvent_1;
    [SerializeField] private RobotEventScript robotEvent_2;

    //  Robots DBController Scripts
    private RobotDBController robotDBController_1;
    private RobotDBController robotDBController_2;

    [HideInInspector] public gameStateEnum gameState;


    //
    bool deadAnimationCoroutineReturn = false;

    private void Awake()
    {
        robotDBController_1 = robotEvent_1.dBController;
        robotDBController_2 = robotEvent_2.dBController;

        Time.timeScale = timeScale;

        if (muteAudio)
        {
            audioManager.enabled = false;
        }

        gameState = gameStateEnum.GamePending;
    }

    private void Update()
    {
        //  Start game
        if (gameState == gameStateEnum.GamePending && robotEvent_1.robotGameState == robotGameStateEnum.Ready && robotEvent_2.robotGameState == robotGameStateEnum.Ready)
        {
            robotEvent_1.enemy = robotEvent_2.GetComponent<RobotEventScript>().robot;
            robotEvent_1.enemyWeapon = robotEvent_2.GetComponent<RobotEventScript>().robotWeapon;

            robotEvent_2.enemy = robotEvent_1.GetComponent<RobotEventScript>().robot;
            robotEvent_2.enemyWeapon = robotEvent_1.GetComponent<RobotEventScript>().robotWeapon;

            gameState = gameStateEnum.GameStarted;
        }

        //  End game
        if (robotEvent_1.robotGameState == robotGameStateEnum.Dead || robotEvent_2.robotGameState == robotGameStateEnum.Dead)
        {
            if (gameType.Equals(gameTypeEnum.GameTest))
            {
                SceneManager.LoadScene("Arena", LoadSceneMode.Single);
            }

            GameObject[] loc_bulletsToDestroy = GameObject.FindGameObjectsWithTag("BulletTag");
            
            foreach (GameObject bullet in loc_bulletsToDestroy)
            {
                Destroy(bullet);
            }

            if (gameState == gameStateEnum.GameStarted && robotEvent_1.robotGameState == robotGameStateEnum.Dead)
            {
                robotDBController_1.RobotUpdateWinrate(robotEvent_1.robotId, false, robotDBController_1);
                robotDBController_2.RobotUpdateWinrate(robotEvent_2.robotId, true, robotDBController_2);

                //Debug.Log("Robot 2 Wins");
            }

            if (gameState == gameStateEnum.GameStarted && robotEvent_2.robotGameState == robotGameStateEnum.Dead)
            {
                robotDBController_1.RobotUpdateWinrate(robotEvent_1.robotId, true, robotDBController_1);
                robotDBController_2.RobotUpdateWinrate(robotEvent_2.robotId, false, robotDBController_2);

                //Debug.Log("Robot 1 Wins");
            }

            StartCoroutine(FinishedGameCoroutine(4));
            gameState = gameStateEnum.GameFinished;
        }

        //  Reload Scene if game is not a test run
        if (!gameType.Equals(gameTypeEnum.GameTest) && gameState == gameStateEnum.GameFinished && robotDBController_1.updateRobotWinrateReturn && robotDBController_2.updateRobotWinrateReturn && deadAnimationCoroutineReturn)
        {
            SceneManager.LoadScene("Arena", LoadSceneMode.Single);
        }
    }

    // Waiting Coroutine
    IEnumerator FinishedGameCoroutine(int arg_waitingTime)
    {
        yield return new WaitForSeconds(arg_waitingTime);
        deadAnimationCoroutineReturn = true;
    }
}